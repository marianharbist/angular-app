import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'repeat'
})
export class RepeatPipe implements PipeTransform {

  transform(value: number): number[] {
    return Array(value).fill('').map((val, index) => index);
  }

}
