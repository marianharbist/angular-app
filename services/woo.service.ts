import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {WooProductModel} from './models/woo-product.model';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {WooOrderModel} from './models/woo-order.model';

@Injectable({
  providedIn: 'root'
})
export class WooService {

  private STORE_KEY = 'WOO_PRODUCTS';

  constructor(
    protected httpClient: HttpClient
  ) { }

  public getProducts(categoryId?: number): Observable<WooProductModel[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Basic '+ btoa(`${WOO_USERNAME}:${WOO_PASS}`)
      })
    }
    return this.httpClient
      .get(`${WOO_BASE_URL}/products?per_page=100${categoryId ? ('&category=' + categoryId) : ''}`, httpOptions)
      .pipe(map((res: any[]) => {
        return res as WooProductModel[]
      }));
  }

  public retrieveOrder(orderId: number): Observable<WooOrderModel> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Basic '+ btoa(`${WOO_USERNAME}:${WOO_PASS}`)
      })
    }
    return this.httpClient
      .get(`${WOO_BASE_URL}/orders/${orderId}`, httpOptions)
      .pipe(map((res: any) => res as WooOrderModel));
  }
}
