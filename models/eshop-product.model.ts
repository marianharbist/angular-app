export class EshopProduct {
  productId: number;
  courseId: number;
  name: string;
  image: string;
  language: LANGUAGE_TYPES;
  languageEshopCode: number;
  languageIso: string;
  studentLanguage: string;
  priority: number;
  publicOffer: boolean;
  activeTrial?: boolean;
  activeFull?: boolean;
  appStoreId: string;

  eshopProductId: number;
  expireDate?: Date;
  isTrial: boolean;

  learnedWords: number;
  allWords: number;
  completedPercent: number;
  lastActivity?: Date;
  success: number;
  time: number;

  isWooProduct?: boolean;
  content?: CourseContentResponse
  details?: ProductShopDetails;
  price?: string;
  subscription?: boolean;

  type: LANGUAGE_TYPES;
}
