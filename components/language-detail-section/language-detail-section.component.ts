import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-language-detail-section',
  templateUrl: './language-detail-section.component.html',
  styleUrls: ['./language-detail-section.component.scss']
})
export class LanguageDetailSectionComponent implements OnInit {
  loading = true;
  courses: EshopProduct[] = [];
  language = '';
  public coursePrices: { [key: number]: string } = {};
  showAllCourses = false;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eshopApi: EshopApi
  ) {
  }

  ngOnInit() {
    this.loading = true;
    this.activatedRoute
      .queryParams
      .subscribe(val => {
        if (val.courses) {
          this.courses = JSON.parse(val.courses);
          this.courses = this.shuffleCourses(this.courses);
        }
        if (val.language) {
          this.language = val.language;
        }
        if (val.prices) {
          this.coursePrices = JSON.parse(val.prices);
        }
        this.loading = false;
      })
  }

  shuffleCourses(courses: EshopProduct[]) {
    for (let i = courses.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [courses[i], courses[j]] = [courses[j], courses[i]];
    }
    return courses;
  }

  async openCourseDetail(course) {
    const price = this.coursePrices[course.productId]
    const data = await this.eshopApi.productShopDetails(course.productId);
    this.router.navigate(['/eshop/course/' + course.id], {
      queryParams: {course: JSON.stringify(course), content: JSON.stringify(data), price},
      skipLocationChange: true
    });
  }

  async openWooCourseDetail(course: AvailableCourse) {
    const price = course.price;
    const data = await this.eshopApi.productShopDetails(course.eshopProductId)
    this.router.navigate(['/eshop/course/' + course.courseId], {
      queryParams: {course: JSON.stringify(course), content: JSON.stringify(data), price},
      skipLocationChange: true
    });
  }

  backButtonClicked() {
    this.router.navigate(['/eshop']);
  }

}
