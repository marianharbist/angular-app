import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-card',
  templateUrl: './flip-card.component.html',
  styleUrls: ['./flip-card.component.scss'],
  animations: [
    flipCardAnimation,
    rotateCardAnimation
  ]
})

export class FlipCardComponent implements OnInit, AfterViewInit {
  currentCardState = flipCardStates.front;
  rotateCardState = rotateStates.static;

  @Input() isDraggable = false;
  @Output() swipeLeftEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() swipeRightEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() {
  }

  ngOnInit() {}

  ngAfterViewInit() {
  }

  toggleCardState() {
    this.currentCardState === flipCardStates.front ?
      this.currentCardState = flipCardStates.back : this.currentCardState = flipCardStates.front;
  }

  rotateLeftAnimation() {
    this.rotateCardState = rotateStates.rotateLeft;
  }

  rotateRightAnimation() {
    this.rotateCardState = rotateStates.rotateRight;
  }

  staticAnimation() {
    this.rotateCardState = rotateStates.static;
  }

  leftAnimationEvent() {
    this.swipeLeftEvent.emit(true);
  }

  rightAnimationEvent() {
    this.swipeRightEvent.emit(true);
  }
}
